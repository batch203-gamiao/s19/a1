console.log("Hello World");

// Login Function Start
let username;
let password;
let role;

function loginUser(){
	username = prompt("Enter your username:");
	password = prompt("Enter your password:");
	role = prompt("Enter your role");

	
	if(username == null || password == null || role == null){
		alert("Input must not be empty.");
	}
	else if(username.length == 0 || password.length == 0 || role.length == 0){
		alert("Input must not be empty.");
	}
	else {
		role = role.toLowerCase();
		switch(role){
			case "admin":
				alert("Welcome back to the class portal, admin!");
				break; 
			case "teacher":
				alert("Thank you for logging in, teacher!");
				break; 
			case "student":
				alert("Welcome to the class portal, student!");
				break; 
			default:
				alert("Role out of range");
				break;	
		}
	}
}

loginUser();
// Login Function End

// Score Equivalent Start
function calcScore(firstNumber,secondNumber,thirdNumber,fourthNumber){

	avg = Math.round((firstNumber+secondNumber+thirdNumber+fourthNumber) / calcScore.length);

	if(role === "admin" || role === "teacher"){
		if(avg <= 74){
		console.log("Your role is " + role + ", the student's average is: " + avg + ". The letter equivalent is F");
		}
		else if(avg >= 75 && avg <= 79){
			console.log("Your role is " + role + ", the student's average is: " + avg + ". The letter equivalent is D");
		}
		else if(avg >= 80 && avg <= 84){
			console.log("Your role is " + role + ", the student's average is: " + avg + ". The letter equivalent is C");
		}
		else if(avg >= 85 && avg <= 89){
			console.log("YYour role is " + role + ", the student's average is: " + avg + ". The letter equivalent is B");
		}
		else if(avg >= 90 && avg <= 96){
			console.log("Your role is " + role + ", the student's average is: " + avg + ". The letter equivalent is A");
		}
		else if(avg >= 96 && avg <= 100){
			console.log("Your role is " + role + ", the student's average is: " + avg + ". The letter equivalent is A+");
		}
		else {
			console.log("Equivalent Score is undefined maybe because the average score is negative or greater than 100");
		}
	}

	else if(role === "student"){
		if(avg <= 74){
		console.log("Hello, " + role + ", your average is: " + avg + ". The letter equivalent is F");
		}
		else if(avg >= 75 && avg <= 79){
			console.log("Hello, " + role + ", your average is: " + avg + ". The letter equivalent is D");
		}
		else if(avg >= 80 && avg <= 84){
			console.log("Hello, " + role + ", your average is: " + avg + ". The letter equivalent is C");
		}
		else if(avg >= 85 && avg <= 89){
			console.log("Hello, " + role + ", your average is: " + avg + ". The letter equivalent is B");
		}
		else if(avg >= 90 && avg <= 96){
			console.log("Hello, " + role + ", your average is: " + avg + ". The letter equivalent is A");
		}
		else if(avg >= 96 && avg <= 100){
			console.log("Hello, " + role + ", your average is: " + avg + ". The letter equivalent is A+");
		}
		else {
			console.log("Equivalent Score is undefined maybe because the average score is negative or greater than 100");
		}
	}

	else {
		console.warn("The grades is accessible only by student, teacher and admin!")
	}
	
}

calcScore(76,79,75,75);


// Score Equivalent End
